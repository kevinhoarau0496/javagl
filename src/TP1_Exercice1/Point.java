package TP1_Exercice1;

/**
 * Created by kevin on 29/09/18.
 */
public class Point {

    private int x;
    private int y;

    public Point() {
        this.x = 0;
        this.y = 0;
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return "Point x=" + x + ", y=" + y;
    }
}
