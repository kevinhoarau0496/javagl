package TP1_Exercice1;

/**
 * Created by kevin on 29/09/18.
 */
public class Rectangle {

    private int longueur;
    private int largeur;
    private Point origine;

    public Rectangle(Point origine) {
        this.origine = origine;
        this.longueur = 0;
        this.largeur = 0;
    }

    public Rectangle(int longueur, int largeur) {
        this.longueur = longueur;
        this.largeur = largeur;
        this.origine = new Point();
    }

    public int getPerimetre() {
        return (longueur * 2) + (largeur * 2);
    }

    public int getSurface() {
        return longueur*largeur;
    }

    public int getLongueur() {
        return longueur;
    }

    public int getLargeur() {
        return largeur;
    }

    public void setLongueur(int longueur) {
        this.longueur = longueur;
    }

    public void setLargeur(int largeur) {
        this.largeur = largeur;
    }

    public String affiche() {
        String str;

        str = "+";
        for(int i = 0 ; i < longueur ; ++i)
            str += "--";
        str += "+\n";

        for(int i = 0 ; i < largeur ; ++i)
        {
            str += "|";
            for(int j = 0 ; j < longueur ; ++j)
                str += "  ";
            str += "|\n";
        }

        str += "+";
        for(int i = 0 ; i < longueur ; ++i)
            str += "--";
        str += "+\n";

        return str;
    }
}
