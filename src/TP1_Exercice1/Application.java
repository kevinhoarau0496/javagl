package TP1_Exercice1;

/**
 * Created by kevin on 29/09/18.
 */
public class Application {

    public static void main(String[] args) {
        Point p1 = new Point(4, 4);
        Point p2 = new Point(1, 6);
        Point p3 = new Point(2, 0);

        System.out.println("Point P1 : " + p1);
        System.out.println("Point P2 : " + p2);
        System.out.println("Point P3 : " + p3);

        Rectangle r1 = new Rectangle(5, 2);
        System.out.println("\nRectangle R1 : L:" + r1.getLongueur() + " l:" + r1.getLargeur());
        System.out.println(r1.affiche());
        System.out.println("Surface : " + r1.getSurface());
        System.out.println("Périmetre : " + r1.getPerimetre());

        Carre c1 = new Carre(8);
        System.out.println("\nCarre C1 : L:" + c1.getLongueur() + " l:" + c1.getLargeur());
        System.out.println(c1.affiche());
        System.out.println("Surface : " + c1.getSurface());
        System.out.println("Périmetre : " + c1.getPerimetre());
    }
}
