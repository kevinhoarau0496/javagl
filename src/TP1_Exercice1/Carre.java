package TP1_Exercice1;

/**
 * Created by kevin on 29/09/18.
 */
public class Carre extends Rectangle {

    public Carre(Point origine) {
        super(origine);
    }

    public Carre(int longueur) {
        super(longueur, longueur);
    }

}
