package TP1_Exercice2;

/**
 * Created by kevin on 29/09/18.
 */
public class Carre extends Rectangle {

    public Carre(int longueur) {
        super(longueur, longueur);
    }

    @Override
    public void setLongueur(int longueur) {
        super.setLongueur(longueur);
        super.setLargeur(longueur);
    }

    @Override
    public void setLargeur(int largeur) {
        super.setLargeur(largeur);
        super.setLongueur(largeur);
    }
}
