package TP1_Exercice2;

/**
 * Created by kevin on 29/09/18.
 */
public class Ellipse extends Figure {

    private int grandAxe;
    private int petitAxe;

    public Ellipse(int grandAxe, int petitAxe) {
        super(new Point(0, 0));
        this.grandAxe = grandAxe;
        this.petitAxe = petitAxe;
    }

    public double getPerimetre() {
        return Math.PI * (grandAxe + petitAxe);
    }

    public double getSurface() {
        return Math.PI * grandAxe * petitAxe;
    }
}
