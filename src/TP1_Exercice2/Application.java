package TP1_Exercice2;

/**
 * Created by kevin on 29/09/18.
 */
public class Application {

    public static void main(String[] args) {
        Rectangle r1 = new Rectangle(10, 3);
        System.out.println("### Rectangle R1 [" + r1.getLargeur() + ';' + r1.getLongueur() + ']');
        System.out.println(r1);
        System.out.println("Surface : " + r1.getSurface());
        System.out.println("Perimetre : " + r1.getPerimetre());

        Carre c1 = new Carre(2);
        System.out.println("### Carre C1 [" + c1.getLargeur() + ';' + c1.getLongueur() + ']');
        System.out.println(c1);
        System.out.println("Surface : " + c1.getSurface());
        System.out.println("Perimetre : " + c1.getPerimetre());
        c1.setLongueur(7);

        System.out.println("### Carre C1 [" + c1.getLargeur() + ';' + c1.getLongueur() + ']');
        System.out.println(c1);
        System.out.println("Surface : " + c1.getSurface());
        System.out.println("Perimetre : " + c1.getPerimetre());
    }
}
