package TP1_Exercice5;

/**
 * Created by kevin on 29/09/18.
 */
public class EmployeA extends Employe {

    protected static final int salaireHeure = 11;
    protected static final int heureLimit = 39;
    private static final double heureSuppPourcentage = 0.3;


    public EmployeA(String nom) {
        super(nom);
    }

    public EmployeA(String nom, int infosSalaire) {
        super(nom, infosSalaire);
    }

    @Override
    public double getSalaire() {
        if(infosSalaire > heureLimit)
            return (heureLimit * salaireHeure + (infosSalaire - heureLimit) * salaireHeure * (heureSuppPourcentage + 1)) * 4;
        else
            return salaireHeure * infosSalaire * 4;
    }
}
