package TP1_Exercice5;

/**
 * Created by kevin on 05/10/18.
 */
public class Paie {

    public static void main(String[] args) {
        Employe[] emps = new Employe[5];

        emps[0] = new Commercial("Commercial 1", 1200);
        emps[1] = new Commercial("Commercial 2", 2290);
        emps[2] = new Commercial("Commercial 3");
        emps[2].setInfosSalaire(1100);

        emps[3] = new EmployeA("EmployeA - 1");
        emps[3].setInfosSalaire(50);

        emps[4] = new EmployeB("EmployeB - 1", 38);

        for(Employe e : emps)
            System.out.println(e.getNom() + " gagne " + e.getSalaire() + "€");
    }
}
