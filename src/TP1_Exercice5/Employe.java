package TP1_Exercice5;

/**
 * Created by kevin on 29/09/18.
 */
public abstract class Employe {

    private String nom;
    protected int infosSalaire;

    public Employe(String nom) {
        this.nom = nom;
    }

    public Employe(String nom, int infosSalaire) {
        this.nom = nom;
        this.infosSalaire = infosSalaire;
    }

    /**
     * @return salaire de l'employe
     */
    public abstract double getSalaire();
    /**
     * @return définit les infos salaire de l'employe
     */
    public void setInfosSalaire(int infosSalaire) {
        this.infosSalaire = infosSalaire;
    }

    public String getNom() {
        return nom;
    }
}
