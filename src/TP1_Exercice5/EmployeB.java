package TP1_Exercice5;

/**
 * Created by kevin on 29/09/18.
 */
public class EmployeB extends EmployeA {

    private static final double heureSuppPourcentage = 0.5;

    public EmployeB(String nom) {
        super(nom);
    }

    public EmployeB(String nom, int infosSalaire) {
        super(nom, infosSalaire);
    }

    @Override
    public double getSalaire() {
        if(infosSalaire > heureLimit)
            return (heureLimit * salaireHeure + (infosSalaire - heureLimit) * salaireHeure * (heureSuppPourcentage + 1)) * 4;
        else
            return salaireHeure * infosSalaire * 4;
    }
}
