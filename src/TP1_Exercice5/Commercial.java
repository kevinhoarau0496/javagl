package TP1_Exercice5;

/**
 * Created by kevin on 29/09/18.
 */
public class Commercial extends Employe {

    private static final int salaireFix = 1200;
    private static final double pourcentageCom = 0.1;

    public Commercial(String nom) {
        super(nom);
    }

    public Commercial(String nom, int infosSalaire) {
        super(nom, infosSalaire);
    }

    @Override
    public double getSalaire() {
        return salaireFix + (infosSalaire * 0.01 * 4);
    }
}
