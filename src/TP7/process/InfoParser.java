package TP7.process;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by kevin on 27/10/18.
 */
public class InfoParser {

    private ArrayList<String> infos;

    public InfoParser(String data) {
        infos = new ArrayList<>();
        Collections.addAll(infos, data.split(";"));
    }

    public ArrayList<String> getInfos() {
        return infos;
    }
}
