package TP7.process;

import TP7.utils.EVar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by kevin on 27/10/18.
 */
public class IPInfoService {

    private static IPInfoService instance;

    private IPInfoService() {

    }

    public String getData(String ip) {
        String urlFormed = "http://api.ipinfodb.com/v3/ip-city/?key=" + EVar.KEYS.getValue() + "&ip=" + ip;
        System.out.println(urlFormed);
        URL url;

        String line = "";
        try {
            url = new URL(urlFormed);
            URLConnection conn = url.openConnection();

            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            line = rd.readLine();
            rd.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return line;
    }

    public static IPInfoService getInstance() {
        if(instance == null)
            instance = new IPInfoService();
        return instance;
    }
}
