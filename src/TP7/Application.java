package TP7;

import TP7.gui.MainFrame;
import TP7.process.IPInfoService;

/**
 * Created by kevin on 27/10/18.
 */
public class Application {

    private static Application instance;
    private IPInfoService ipInfoService;

    private Application() {
        System.out.println("[CONTROLER] INIT");
        ipInfoService = IPInfoService.getInstance();
        MainFrame.getInstance();
        System.out.println("[CONTROLER] OK");
    }

    public String getData(String ip) {
        return ipInfoService.getData(ip);
    }

    public static Application getInstance() {
        if(instance == null)
            instance = new Application();
        return instance;
    }

    public static void main(String[] args) {
        Application.getInstance();
    }
}
