package TP7.gui;

import TP7.Application;
import TP7.process.InfoParser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by kevin on 27/10/18.
 */
public class MainFrame extends JFrame implements ActionListener {

    private static MainFrame instance;

    private JTextField jtfIpAddress;
    private JTextField jtfCountryCode;
    private JTextField jtfCountryName;
    private JTextField jtfRegionName;
    private JTextField jtfCityName;
    private JTextField jtfZipCode;
    private JTextField jtfLatitude;
    private JTextField jtfLongitude;
    private JTextField jtfTimeZone;

    private MainFrame() {
        super("IPInfoDBClient");
        initGui();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void initGui() {
        JPanel mainPanel = new JPanel(new BorderLayout());

        JPanel northPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JLabel northLabelTitle = new JLabel("My IP Locator v1.0");
        northPanel.add(northLabelTitle);

        JPanel centerPanel = new JPanel(new GridLayout(9, 2));
        //IP ADDRESS
        JLabel labelIpAddress = new JLabel("ipAddress");
        jtfIpAddress = new JTextField();
        centerPanel.add(labelIpAddress);
        centerPanel.add(jtfIpAddress);

        //IP ADDRESS
        JLabel labelCountryCode = new JLabel("countryCode");
        jtfCountryCode = new JTextField();
        jtfCountryCode.setEnabled(false);
        centerPanel.add(labelCountryCode);
        centerPanel.add(jtfCountryCode);

        //IP ADDRESS
        JLabel labelCountryName = new JLabel("countryName");
        jtfCountryName = new JTextField();
        jtfCountryName.setEnabled(false);
        centerPanel.add(labelCountryName);
        centerPanel.add(jtfCountryName);

        //IP ADDRESS
        JLabel labelRegionName = new JLabel("regionName");
        jtfRegionName = new JTextField();
        jtfRegionName.setEnabled(false);
        centerPanel.add(labelRegionName);
        centerPanel.add(jtfRegionName);

        //IP ADDRESS
        JLabel labelCityName = new JLabel("cityName");
        jtfCityName = new JTextField();
        jtfCityName.setEnabled(false);
        centerPanel.add(labelCityName);
        centerPanel.add(jtfCityName);

        //IP ADDRESS
        JLabel labelZipCode = new JLabel("zipCode");
        jtfZipCode = new JTextField();
        jtfZipCode.setEnabled(false);
        centerPanel.add(labelZipCode);
        centerPanel.add(jtfZipCode);

        //IP ADDRESS
        JLabel labelLatitude = new JLabel("latitude");
        jtfLatitude = new JTextField();
        jtfLatitude.setEnabled(false);
        centerPanel.add(labelLatitude);
        centerPanel.add(jtfLatitude);

        //IP ADDRESS
        JLabel labelLongitude = new JLabel("longitude");
        jtfLongitude = new JTextField();
        jtfLongitude.setEnabled(false);
        centerPanel.add(labelLongitude);
        centerPanel.add(jtfLongitude);

        //IP ADDRESS
        JLabel labelTimeZone = new JLabel("timeZone");
        jtfTimeZone = new JTextField();
        jtfTimeZone.setEnabled(false);
        centerPanel.add(labelTimeZone);
        centerPanel.add(jtfTimeZone);

        JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JButton btnSpecIp = new JButton("Get info with specific IP");
        btnSpecIp.addActionListener(this);
        JButton btnLocalIp = new JButton("Get info with my local IP");
        btnLocalIp.addActionListener(this);
        southPanel.add(btnSpecIp);
        southPanel.add(btnLocalIp);

        mainPanel.add(northPanel, BorderLayout.NORTH);
        mainPanel.add(centerPanel, BorderLayout.CENTER);
        mainPanel.add(southPanel, BorderLayout.SOUTH);

        setContentPane(mainPanel);
        pack();
        setVisible(true);
    }

    public static MainFrame getInstance() {
        if(instance == null)
            instance = new MainFrame();
        return instance;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String cmd = actionEvent.getActionCommand();

        String dataResult = "";
        switch (cmd)
        {
            case "Get info with specific IP":
            {
                String ip = jtfIpAddress.getText();
                dataResult = Application.getInstance().getData(ip);
                break;
            }
            case "Get info with my local IP":
            {
                try {
                    dataResult = Application.getInstance().getData(InetAddress.getLocalHost().getHostAddress());
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                break;
            }
        }

        InfoParser infoParser = new InfoParser(dataResult);
        jtfIpAddress.setText(infoParser.getInfos().get(2));
        jtfCountryCode.setText(infoParser.getInfos().get(3));
        jtfCountryName.setText(infoParser.getInfos().get(4));
        jtfRegionName.setText(infoParser.getInfos().get(5));
        jtfCityName.setText(infoParser.getInfos().get(6));
        jtfZipCode.setText(infoParser.getInfos().get(7));
        jtfLatitude.setText(infoParser.getInfos().get(8));
        jtfLongitude.setText(infoParser.getInfos().get(9));
        jtfTimeZone.setText(infoParser.getInfos().get(10));
    }
}
