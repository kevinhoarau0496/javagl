package TP7.utils;

/**
 * Created by kevin on 27/10/18.
 */
public enum EVar {

    KEYS("d57b1e68028f1e5629d553346e48f7f2457981a385726f9c6daf4ddff36f5eb6");

    private String value;

    EVar(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
