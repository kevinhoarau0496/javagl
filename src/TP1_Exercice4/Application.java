package TP1_Exercice4;

import java.util.Random;

/**
 * Created by kevin on 29/09/18.
 */
public class Application {

    public static void main(String[] args) {
        int size = Integer.parseInt(args[0]);
        Figure[] tf = new Figure[size];

        Random randomType = new Random();
        Random randomValue = new Random();

        for(int i = 0 ; i < size ; ++i)
        {
            int rand = randomType.nextInt(4);
            switch(rand)
            {
                case 0:
                    tf[i] = new Ellipse(10 * randomValue.nextDouble(), 10 * randomValue.nextDouble());
                    break;
                case 1:
                    tf[i] = new Cercle(10 * randomValue.nextDouble());
                    break;
                case 2:
                    tf[i] = new Rectangle(10 * randomValue.nextDouble(), 10 * randomValue.nextDouble());
                    break;
                case 3:
                    tf[i] = new Carre(10 * randomValue.nextDouble());
                    break;
            }

            System.out.println("Surface : " + tf[i].getSurface());
            System.out.println("Perimetre : " + tf[i].getPerimetre());
            System.out.println();
        }
    }
}
