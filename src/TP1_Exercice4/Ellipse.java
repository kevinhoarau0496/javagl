package TP1_Exercice4;

/**
 * Created by kevin on 29/09/18.
 */
public class Ellipse extends Figure {

    private double grandAxe;
    private double petitAxe;

    public Ellipse(double grandAxe, double petitAxe) {
        super(new Point(0, 0));
        this.grandAxe = grandAxe;
        this.petitAxe = petitAxe;
    }

    public double getPerimetre() {
        return Math.PI * (grandAxe + petitAxe);
    }

    public double getSurface() {
        return Math.PI * grandAxe * petitAxe;
    }
}
