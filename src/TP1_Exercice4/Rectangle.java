package TP1_Exercice4;

/**
 * Created by kevin on 29/09/18.
 */
public class Rectangle extends Figure {

    private double longueur;
    private double largeur;

    public Rectangle(double longueur, double largeur) {
        super(new Point(0, 0));
        this.longueur = longueur;
        this.largeur = largeur;
    }

    public double getPerimetre() {
        return (longueur * 2) + (largeur * 2);
    }

    public double getSurface() {
        return longueur * largeur;
    }

    public double getLongueur() {
        return longueur;
    }

    public double getLargeur() {
        return largeur;
    }

    public void setLongueur(double longueur) {
        this.longueur = longueur;
    }

    public void setLargeur(double largeur) {
        this.largeur = largeur;
    }

    @Override
    public String toString() {
        String str;

        str = "+";
        for(int i = 0 ; i < longueur ; ++i)
            str += "--";
        str += "+\n";

        for(int i = 0 ; i < largeur ; ++i)
        {
            str += "|";
            for(int j = 0 ; j < longueur ; ++j)
                str += "  ";
            str += "|\n";
        }

        str += "+";
        for(int i = 0 ; i < longueur ; ++i)
            str += "--";
        str += "+";

        return str;
    }
}
