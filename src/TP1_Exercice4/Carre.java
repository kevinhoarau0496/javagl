package TP1_Exercice4;

/**
 * Created by kevin on 29/09/18.
 */
public class Carre extends Rectangle {

    public Carre(double longueur) {
        super(longueur, longueur);
    }

    @Override
    public void setLongueur(double longueur) {
        super.setLongueur(longueur);
        super.setLargeur(longueur);
    }

    @Override
    public void setLargeur(double largeur) {
        super.setLargeur(largeur);
        super.setLongueur(largeur);
    }
}
