package TP3_Exercice2;

import java.awt.*;

/**
 * Created by kevin on 29/09/18.
 */
public class Ellipse extends Figure {

    protected double grandAxe;
    protected double petitAxe;

    public Ellipse(double grandAxe, double petitAxe) {
        super(new Point(0, 0));
        this.grandAxe = grandAxe;
        this.petitAxe = petitAxe;
    }

    public Ellipse(int px, int py, Color color) {
        super(new Point(px, py), color);
        this.grandAxe = 0;
        this.petitAxe = 0;
    }

    public double getPerimetre() {
        return Math.PI * (grandAxe + petitAxe);
    }

    public double getSurface() {
        return Math.PI * grandAxe * petitAxe;
    }

    @Override
    public void setBoundingBox(int hauteurBB, int largeurBB) {
        grandAxe = largeurBB;
        petitAxe = hauteurBB;
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(color);
        g.fillOval(origine.getX(), origine.getY(), (int) petitAxe, (int) grandAxe);
    }
}
