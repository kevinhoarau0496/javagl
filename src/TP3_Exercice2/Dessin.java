package TP3_Exercice2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

/**
 * Created by kevin on 06/10/18.
 */
public class Dessin extends JPanel implements MouseListener, MouseMotionListener {

    private ArrayList<Figure> figures;
    private Figure currentDraw;
    private Color color;
    private String nomFigure;

    private int posX;
    private int posY;

    public Dessin() {
        super();

        this.figures = new ArrayList<>();
        this.currentDraw = null;
        this.color = Color.BLACK;
        this.nomFigure = "Rectangle";

        setBackground(Color.WHITE);
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setNomFigure(String nomFigure) {
        this.nomFigure = nomFigure;
    }

    public ArrayList<Figure> getFigures() {
        return figures;
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        for(Figure f : figures)
            f.draw(graphics);

        if(currentDraw != null)
            currentDraw.draw(graphics);
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        posX = mouseEvent.getX();
        posY = mouseEvent.getY();
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        int dX = mouseEvent.getX();
        int dY = mouseEvent.getY();

        int x = Math.min(posX, dX);
        int y = Math.min(posY, dY);
        int width = Math.abs(posX - dX);
        int height = Math.abs(posY - dY);

        Figure f = null;
        switch (nomFigure)
        {
            case "Ellipse":
                f = new Ellipse(x, y, color);
                break;
            case "Cercle":
                f = new Cercle(x, y, color);
                break;
            case "Rectangle":
                f = new Rectangle(x, y, color);
                break;
            case "Carre":
                f = new Carre(x, y, color);
                break;
        }

        assert f != null;
        f.setBoundingBox(width, height);
        figures.add(f);
        currentDraw = null;
        paintComponent(getGraphics());
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        int dX = mouseEvent.getX();
        int dY = mouseEvent.getY();

        int x = Math.min(posX, dX);
        int y = Math.min(posY, dY);
        int width = Math.abs(posX - dX);
        int height = Math.abs(posY - dY);

        switch (nomFigure)
        {
            case "Ellipse":
                currentDraw = new Ellipse(x, y, color);
                break;
            case "Cercle":
                currentDraw = new Cercle(x, y, color);
                break;
            case "Rectangle":
                currentDraw = new Rectangle(x, y, color);
                break;
            case "Carre":
                currentDraw = new Carre(x, y, color);
                break;
        }

        assert currentDraw != null;
        currentDraw.setBoundingBox(width, height);
        paintComponent(getGraphics());
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {

    }
}
