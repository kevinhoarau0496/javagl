package TP3_Exercice2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 * Created by kevin on 06/10/18.
 */
public class Fenetre extends JFrame implements ActionListener {

    private Dessin dessin;

    public Fenetre(String titre, Dessin dessin) {
        super(titre);
        this.dessin = dessin;

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        initMenu();
        initGui();
    }

    private void initMenu() {
        JMenuBar menuBar = new JMenuBar();
        JMenu mFichier = new JMenu("Fichier");
        JMenu mPropos = new JMenu("A Propos");

        JMenuItem miNouveau = new JMenuItem("Nouveau");
        miNouveau.addActionListener(this);

        JMenuItem miOuvrir = new JMenuItem("Ouvrir");
        miOuvrir.addActionListener(this);

        JMenuItem miSauvegarder = new JMenuItem("Sauvegarder");
        miSauvegarder.addActionListener(this);
        miSauvegarder.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));

        JMenuItem miQuitter = new JMenuItem("Quitter");
        miQuitter.addActionListener(this);

        JMenuItem miAuteur = new JMenuItem("Auteur");
        miAuteur.addActionListener(this);

        mFichier.add(miNouveau);
        mFichier.add(miOuvrir);
        mFichier.add(miSauvegarder);
        mFichier.addSeparator();
        mFichier.add(miQuitter);
        mPropos.add(miAuteur);

        menuBar.add(mFichier);
        menuBar.add(mPropos);

        setJMenuBar(menuBar);
    }

    private void initGui() {
        setLayout(new BorderLayout());
        add(dessin, BorderLayout.CENTER);

        JPanel southPanel = new JPanel(new GridLayout(1, 2));
        JPanel couleurPanel = new JPanel(new GridLayout(2, 4));
        JPanel formePanel = new JPanel(new GridLayout(2, 2));

        JButton btnNoir = new JButton("Noir");
        btnNoir.setBackground(Color.BLACK);
        btnNoir.addActionListener(this);
        couleurPanel.add(btnNoir);

        JButton btnRouge = new JButton("Rouge");
        btnRouge.setBackground(Color.RED);
        btnRouge.addActionListener(this);
        couleurPanel.add(btnRouge);

        JButton btnVert = new JButton("Vert");
        btnVert.setBackground(Color.GREEN);
        btnVert.addActionListener(this);
        couleurPanel.add(btnVert);

        JButton btnBleu = new JButton("Bleu");
        btnBleu.setBackground(Color.BLUE);
        btnBleu.addActionListener(this);
        couleurPanel.add(btnBleu);

        JButton btnRose = new JButton("Rose");
        btnRose.setBackground(Color.PINK);
        btnRose.addActionListener(this);
        couleurPanel.add(btnRose);

        JButton btnJaune = new JButton("Jaune");
        btnJaune.setBackground(Color.YELLOW);
        btnJaune.addActionListener(this);
        couleurPanel.add(btnJaune);

        JButton btnMagenta = new JButton("Magenta");
        btnMagenta.setBackground(Color.MAGENTA);
        btnMagenta.addActionListener(this);
        couleurPanel.add(btnMagenta);

        JButton btnOrange = new JButton("Orange");
        btnOrange.setBackground(Color.ORANGE);
        btnOrange.addActionListener(this);
        couleurPanel.add(btnOrange);

        JButton btnEllipse = new JButton("Ellipse");
        btnEllipse.addActionListener(this);
        formePanel.add(btnEllipse);

        JButton btnCercle = new JButton("Cercle");
        btnCercle.addActionListener(this);
        formePanel.add(btnCercle);

        JButton btnCarre = new JButton("Carre");
        btnCarre.addActionListener(this);
        formePanel.add(btnCarre);

        JButton btnRectangle = new JButton("Rectangle");
        btnRectangle.addActionListener(this);
        formePanel.add(btnRectangle);

        southPanel.add(couleurPanel);
        southPanel.add(formePanel);

        add(southPanel, BorderLayout.SOUTH);

        setLocation(0, 0);
        setSize(1200, 800);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String cmd = actionEvent.getActionCommand();

        switch (cmd)
        {
            case "Ellipse":
                dessin.setNomFigure("Ellipse");
                System.out.println("Forme : Ellipse");
                break;
            case "Cercle":
                dessin.setNomFigure("Cercle");
                System.out.println("Forme : Cercle");
                break;
            case "Carre":
                dessin.setNomFigure("Carre");
                System.out.println("Forme : Carre");
                break;
            case "Rectangle":
                dessin.setNomFigure("Rectangle");
                System.out.println("Forme : Rectangle");
                break;
            case "Noir":
                dessin.setColor(Color.BLACK);
                System.out.println("Couleur : Noir");
                break;
            case "Rouge":
                dessin.setColor(Color.RED);
                System.out.println("Couleur : Rouge");
                break;
            case "Vert":
                dessin.setColor(Color.GREEN);
                System.out.println("Couleur : Vert");
                break;
            case "Bleu":
                dessin.setColor(Color.BLUE);
                System.out.println("Couleur : Bleu");
                break;
            case "Rose":
                dessin.setColor(Color.PINK);
                System.out.println("Couleur : Rose");
                break;
            case "Jaune":
                dessin.setColor(Color.YELLOW);
                System.out.println("Couleur : Jaune");
                break;
            case "Magenta":
                dessin.setColor(Color.MAGENTA);
                System.out.println("Couleur : Magenta");
                break;
            case "Orange":
                dessin.setColor(Color.ORANGE);
                System.out.println("Couleur : Orange");
                break;
            case "Nouveau":
                dessin.getFigures().clear();
                dessin.repaint();
                break;
            case "Auteur":
                new APropos();
                break;
            case "Quitter":
                System.exit(0);
            default:
                System.err.println(cmd);
                break;
        }
    }
}
