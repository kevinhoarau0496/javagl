package TP3_Exercice2;

import java.awt.*;

/**
 * Created by kevin on 29/09/18.
 */
public class Rectangle extends Figure {

    protected double longueur;
    protected double largeur;

    public Rectangle(double longueur, double largeur) {
        super(new Point(0, 0));
        this.longueur = longueur;
        this.largeur = largeur;
    }

    public Rectangle(int px, int py, Color color) {
        super(new Point(px, py), color);
        this.longueur = 0;
        this.largeur = 0;
    }

    public double getPerimetre() {
        return (longueur * 2) + (largeur * 2);
    }

    public double getSurface() {
        return longueur * largeur;
    }

    @Override
    public void setBoundingBox(int hauteurBB, int largeurBB) {
        longueur = hauteurBB;
        largeur = largeurBB;
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(color);
        g.fillRect(origine.getX(), origine.getY(), (int) longueur, (int) largeur);
    }

    public double getLongueur() {
        return longueur;
    }

    public double getLargeur() {
        return largeur;
    }

    public void setLongueur(double longueur) {
        this.longueur = longueur;
    }

    public void setLargeur(double largeur) {
        this.largeur = largeur;
    }

    @Override
    public String toString() {
        String str;

        str = "+";
        for(int i = 0 ; i < longueur ; ++i)
            str += "--";
        str += "+\n";

        for(int i = 0 ; i < largeur ; ++i)
        {
            str += "|";
            for(int j = 0 ; j < longueur ; ++j)
                str += "  ";
            str += "|\n";
        }

        str += "+";
        for(int i = 0 ; i < longueur ; ++i)
            str += "--";
        str += "+";

        return str;
    }
}
