package TP3_Exercice2;

import javax.swing.*;
import java.awt.*;

/**
 * Created by kevin on 06/10/18.
 */
public class APropos extends JFrame {

    public APropos() {
        super("A Propos");
        initGui();
    }

    private void initGui() {
        setLayout(new BorderLayout());
        JLabel label = new JLabel("Kevin HOARAU - ITII3 Opt GL");
        label.setFont(new Font("Serif", Font.PLAIN, 22));
        add(label, BorderLayout.CENTER);
        pack();
        setVisible(true);
    }
}
