package TP3_Exercice2;

import java.awt.*;

/**
 * Created by kevin on 29/09/18.
 */
public class Carre extends Rectangle {

    public Carre(double longueur) {
        super(longueur, longueur);
    }

    public Carre(int px, int py, Color color) {
        super(px, py, color);
    }

    @Override
    public void setLongueur(double longueur) {
        super.setLongueur(longueur);
        super.setLargeur(longueur);
    }

    @Override
    public void setLargeur(double largeur) {
        super.setLargeur(largeur);
        super.setLongueur(largeur);
    }

    @Override
    public void setBoundingBox(int hauteurBB, int largeurBB) {
        longueur = largeur = hauteurBB;
    }
}
