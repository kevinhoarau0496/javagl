package TP3_Exercice2;


import java.awt.*;

/**
 * Created by kevin on 29/09/18.
 */
public abstract class Figure {

    protected Point origine;
    protected Color color;

    public Figure(Point origine) {
        this.origine = origine;
    }

    public Figure(Point origine, Color color) {
        this.origine = origine;
        this.color = color;
    }

    /**
     * @return perimetre de la figure
     */
    public abstract double getPerimetre();
    /**
     * @return surface de la figure
     */
    public abstract double getSurface();

    public abstract void setBoundingBox(int hauteurBB, int largeurBB);

    public abstract void draw(Graphics g);
}
