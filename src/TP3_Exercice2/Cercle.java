package TP3_Exercice2;

import java.awt.*;

/**
 * Created by kevin on 29/09/18.
 */
public class Cercle extends Ellipse {

    public Cercle(double axe) {
        super(axe, axe);
    }

    public Cercle(int px, int py, Color color) {
        super(px, py, color);
    }

    @Override
    public void setBoundingBox(int hauteurBB, int largeurBB) {
        grandAxe = petitAxe = hauteurBB;
    }
}
