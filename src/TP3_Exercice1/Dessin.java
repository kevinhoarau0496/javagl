package TP3_Exercice1;

import javax.swing.*;
import java.awt.*;

/**
 * Created by kevin on 06/10/18.
 */
public class Dessin extends JPanel {

    public Dessin() {
        super();
        setBackground(Color.WHITE);
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        graphics.setColor(Color.RED);
        graphics.drawRect(5, 5, 5, 5);
    }
}
