package TP3_Exercice1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Created by kevin on 06/10/18.
 */
public class Fenetre extends JFrame {

    private Dessin dessin;

    public Fenetre(String titre, Dessin dessin) {
        super(titre);
        this.dessin = dessin;

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        initMenu();
        initGui();
    }

    private void initMenu() {
        JMenuBar menuBar = new JMenuBar();
        JMenu mFichier = new JMenu("Fichier");
        JMenu mPropos = new JMenu("A Propos");

        JMenuItem miNouveau = new JMenuItem("Nouveau");
        JMenuItem miOuvrir = new JMenuItem("Ouvrir");
        JMenuItem miSauvegarder = new JMenuItem("Sauvegarder");
        miSauvegarder.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
        JMenuItem miQuitter = new JMenuItem("Quitter");
        JMenuItem miAuteur = new JMenuItem("Auteur");

        mFichier.add(miNouveau);
        mFichier.add(miOuvrir);
        mFichier.add(miSauvegarder);
        mFichier.addSeparator();
        mFichier.add(miQuitter);
        mPropos.add(miAuteur);

        menuBar.add(mFichier);
        menuBar.add(mPropos);

        setJMenuBar(menuBar);
    }

    private void initGui() {
        setLayout(new BorderLayout());
        add(dessin, BorderLayout.CENTER);

        JPanel southPanel = new JPanel(new GridLayout(1, 2));
        JPanel couleurPanel = new JPanel(new GridLayout(2, 4));
        JPanel formePanel = new JPanel(new GridLayout(2, 2));

        JButton btnNoir = new JButton("Noir");
        btnNoir.setBackground(Color.BLACK);
        couleurPanel.add(btnNoir);
        JButton btnRouge = new JButton("Rouge");
        btnRouge.setBackground(Color.RED);
        couleurPanel.add(btnRouge);
        JButton btnVert = new JButton("Vert");
        btnVert.setBackground(Color.GREEN);
        couleurPanel.add(btnVert);
        JButton btnBleu = new JButton("Bleu");
        btnBleu.setBackground(Color.BLUE);
        couleurPanel.add(btnBleu);
        JButton btnRose = new JButton("Rose");
        btnRose.setBackground(Color.PINK);
        couleurPanel.add(btnRose);
        JButton btnJaune = new JButton("Jaune");
        btnJaune.setBackground(Color.YELLOW);
        couleurPanel.add(btnJaune);
        JButton btnMagenta = new JButton("Magenta");
        btnMagenta.setBackground(Color.MAGENTA);
        couleurPanel.add(btnMagenta);
        JButton btnOrange = new JButton("Orange");
        btnOrange.setBackground(Color.ORANGE);
        couleurPanel.add(btnOrange);

        JButton btnEllipse = new JButton("Ellipse");
        formePanel.add(btnEllipse);
        JButton btnCercle = new JButton("Cercle");
        formePanel.add(btnCercle);
        JButton btnCarre = new JButton("Carre");
        formePanel.add(btnCarre);
        JButton btnRectangle = new JButton("Rectangle");
        formePanel.add(btnRectangle);

        southPanel.add(couleurPanel);
        southPanel.add(formePanel);

        add(southPanel, BorderLayout.SOUTH);

        setLocation(0, 0);
        setSize(1200, 800);
        setVisible(true);
    }

}
