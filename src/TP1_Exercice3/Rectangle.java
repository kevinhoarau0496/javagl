package TP1_Exercice3;

/**
 * Created by kevin on 29/09/18.
 */
public class Rectangle extends Figure {

    private int longueur;
    private int largeur;

    public Rectangle(int longueur, int largeur) {
        super(new Point(0, 0));
        this.longueur = longueur;
        this.largeur = largeur;
    }

    public double getPerimetre() {
        return (longueur * 2) + (largeur * 2);
    }

    public double getSurface() {
        return longueur * largeur;
    }

    public int getLongueur() {
        return longueur;
    }

    public int getLargeur() {
        return largeur;
    }

    public void setLongueur(int longueur) {
        this.longueur = longueur;
    }

    public void setLargeur(int largeur) {
        this.largeur = largeur;
    }

    @Override
    public String toString() {
        String str;

        str = "+";
        for(int i = 0 ; i < longueur ; ++i)
            str += "--";
        str += "+\n";

        for(int i = 0 ; i < largeur ; ++i)
        {
            str += "|";
            for(int j = 0 ; j < longueur ; ++j)
                str += "  ";
            str += "|\n";
        }

        str += "+";
        for(int i = 0 ; i < longueur ; ++i)
            str += "--";
        str += "+";

        return str;
    }
}
