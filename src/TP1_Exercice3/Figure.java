package TP1_Exercice3;

/**
 * Created by kevin on 29/09/18.
 */
public abstract class Figure {

    protected Point origine;

    public Figure(Point origine) {
        this.origine = origine;
    }

    /**
     * @return perimetre de la figure
     */
    public abstract double getPerimetre();
    /**
     * @return surface de la figure
     */
    public abstract double getSurface();
}
