package TP1_Exercice3;

/**
 * Created by kevin on 29/09/18.
 */
public class Application {

    public static void main(String[] args) {
        //V1 (sans polymorphisme)
        /*
        Figure[] tf = new Figure[4];
        tf[0] = new Figure(new Point(2, 3));
        tf[1] = new Rectangle(8, 3);
        tf[2] = new Carre(4);
        tf[3] = new Cercle(4);

        System.out.println("### Point");
        System.out.println(tf[0]);

        System.out.println("### Rectangle [" + ((Rectangle)tf[1]).getLargeur() + ';' + ((Rectangle)tf[1]).getLongueur() + ']');
        System.out.println(((Rectangle)tf[1]));
        System.out.println("Surface : " + ((Rectangle)tf[1]).getSurface());
        System.out.println("Perimetre : " + ((Rectangle)tf[1]).getPerimetre());

        System.out.println("### Carre [" + ((Carre)tf[2]).getLargeur() + ';' + ((Carre)tf[2]).getLongueur() + ']');
        System.out.println(((Carre)tf[2]));
        System.out.println("Surface : " + ((Carre)tf[2]).getSurface());
        System.out.println("Perimetre : " + ((Carre)tf[2]).getPerimetre());

        System.out.println("### Cercle ");
        System.out.println("Surface : " + ((Cercle)tf[3]).getSurface());
        System.out.println("Perimetre : " + ((Cercle)tf[3]).getPerimetre());
        */

        //V2 (avec polymorphisme)
        Figure[] tf = new Figure[4];
        tf[0] = new Ellipse(12, 4);
        tf[1] = new Rectangle(8, 3);
        tf[2] = new Carre(4);
        tf[3] = new Cercle(4);

        System.out.println("### Ellipse ");
        System.out.println("Surface : " + tf[0].getSurface());
        System.out.println("Perimetre : " + tf[0].getPerimetre());

        System.out.println("### Rectangle [" + ((Rectangle)tf[1]).getLargeur() + ';' + ((Rectangle)tf[1]).getLongueur() + ']');
        System.out.println(tf[1]);
        System.out.println("Surface : " + tf[1].getSurface());
        System.out.println("Perimetre : " + tf[1].getPerimetre());

        System.out.println("### Carre [" + ((Carre)tf[2]).getLargeur() + ';' + ((Carre)tf[2]).getLongueur() + ']');
        System.out.println(tf[2]);
        System.out.println("Surface : " + tf[2].getSurface());
        System.out.println("Perimetre : " + tf[2].getPerimetre());

        System.out.println("### Cercle ");
        System.out.println("Surface : " + tf[3].getSurface());
        System.out.println("Perimetre : " + tf[3].getPerimetre());
    }
}
